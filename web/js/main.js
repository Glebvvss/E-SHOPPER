function showCart(cart) {
	$('#cart .modal-body').html(cart);
	$('#cart').modal();
}

/*open cart*/
$('.cart-class').on('click', function(e) {
	e.preventDefault();
	var id = $(this).data('id');
	$.ajax({
		url: '/cart/open',
		data: {id: id},
		type: 'GET',
		success: function(res) {
			showCart(res);
		}
	});
});

/*add to cart*/
$('.add-to-cart').on('click', function(e) {
	e.preventDefault();
	var id = $(this).data('id');
	$.ajax({
		url: '/cart/add',
		data: {id: id},
		type: 'GET',
		success: function(res) {
			if(!res) alert('Error!');
			showCart(res);
		},
		error: function() {
			alert('Error');
		}
	})
});

/*delete from cart*/
$(document).ready(function() {
	$('#cart .modal-body').on('click', '.del-item', function() {
		var id = $(this).data('id');		
		$.ajax({
			url: '/cart/delete',
			data: {id: id},
			type: 'GET',
			success: function(res) {
				if(!res) alert('Error!');
				showCart(res);			
			},
			error: function() {
				alert('Error');
			}
		});	
	});
});

/*qty*/
$(document).ready(function() {
	$('#cart .modal-body').on('change', '.pieces-input', function() {
		var id = $(this).data('id');
		var qty = $(this).val();
		$.ajax({
			url: '/cart/qty-of-item',
			data: {id: id, qty: qty},
			type: 'GET',
			success: function(res) {
				showCart(res);
			}
		});
	});
});

/*select delivary*/
$(document).ready(function() {
	$('#cart .modal-body').on('change', '#select-cart', function() {
		var value = $('#select-cart option:selected').val();		
		$('#confirm-order').attr('data-id', value);
		var id = '#' + value;
		$('.select-delivery').animate({height: 'hide'}, 500);		
		$(id).animate({height: 'show'}, 500); 
	});
});

var validate = function(type_order) {
	if (type_order = 'pickup') {
		var stateRadioBtn = $('input[name=pickup]:checked').val();		
		if (stateRadioBtn == undefined) {			
			alert('Please select office');
			return;
		}
	}

	if (type_order = 'home') {
		var input = $('.confirm-order').attr('value');
		console.log(input);
	}
};

/*confirm order*/
$(document).ready(function() {
	$('#cart .modal-body').on('click', '#confirm-order', function() {
		var type_order = $('#confirm-order').attr('data-id');

		if (type_order == 0) {
			alert('Please select type of delivery your items');
			return;
		}
		validate(type_order);
/*
		$.ajax({
			url: '/cart/confirm-order',
			data: {type_order: type_order},
			type: 'GET',
			success: function(res) {
				showCart(res);
			}
		});
*/
	});
});

/*hide textarea of subcomments*/
$(document).ready(function() {
	$('.replay').hide();
});


/*func for comment replat script*/
function getNumFromDataId(idElement) {
	var id = $(idElement).attr('data-id');
	var idArr = id.split('-');
	return id = idArr['1'];
};

/*func for comment replat script*/
function getNumFromId(idElement) {
	var idArr = idElement.split('-');
	return id = idArr['1'];
};

/*comment replay*/
$(document).ready(function() {
	$(document).on('click', '.submit-comment', function() {	

		var id = getNumFromId($(this).attr('id'));
		var text = $('#text-' + id).val();
		var id_article = getNumFromDataId('#article');

		$.ajax({
			url: '/blog/update-comments-in-article-ajax',
			data: {id: id, id_article: id_article, text: text},
			type: 'POST',
			success: function(res) {
				$("#comments").html(res);
				$('.replay').hide();
				openReplay();
			}
		});
	});
});

/*open replay box*/
var openReplay = function() {
	$(document).ready(function() {
		$('.show-replay').click( function() {		
			$('.replay').hide();
			var idReplay = $(this).attr('id');
			var idReplayArr = idReplay.split('-');		
			$('#com-' + idReplayArr[1]).show();
		});
	});
};
openReplay();

/**/
$(document).ready(function() {
    $('#browse_button').click(function() {
        $('#file_browse').click().change(function() {
            var lastIndexOfSep = this.value.lastIndexOf('\\'); // Windows
            if (lastIndexOfSep == -1)
                lastIndexOfSep = this.value.lastIndexOf('/'); // Linux
            var fileName = this.value.substring(lastIndexOfSep + 1, this.length);
            $("#file_name").val(fileName);
        });
    });
});

/*price range*/
$('#sl2').slider();
	var RGBChange = function() {
	  $('#RGB').css('background', 'rgb('+r.getValue()+','+g.getValue()+','+b.getValue()+')')
	};	
		
/*scroll to top*/
$(document).ready(function(){
	$(function () {
		$.scrollUp({
	        scrollName: 'scrollUp', // Element ID
	        scrollDistance: 300, // Distance from top/bottom before showing element (px)
	        scrollFrom: 'top', // 'top' or 'bottom'
	        scrollSpeed: 300, // Speed back to top (ms)
	        easingType: 'linear', // Scroll to top easing (see http://easings.net/)
	        animation: 'fade', // Fade, slide, none
	        animationSpeed: 200, // Animation in speed (ms)
	        scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
					//scrollTarget: false, // Set a custom target element for scrolling to the top
	        scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
	        scrollTitle: false, // Set a custom <a> title if required.
	        scrollImg: false, // Set true to use image
	        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	        zIndex: 2147483647 // Z-Index for the overlay
		});
	});
});
