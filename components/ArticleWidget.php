<?php 

namespace app\components;

use yii;
use yii\base\Widget;

class ArticleWidget extends Widget {

	public $id_article;
	public $article;
	public $tags;

	public function run() {
		$tpl = $this->getHtml($this->id_article, $this->article, $this->tags);
		return $tpl;
	}

	public function getHtml($id_article, $article, $tags) {
		ob_start();
		include 'templates/ArticleWidgetView.php';
		return ob_get_clean();
	}

}
