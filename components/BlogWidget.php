<?php

namespace app\components;

use yii;
use yii\base\Widget;

class BlogWidget extends Widget {

	public $blog;

	public function run() {
		$tpl = $this->getHtml($this->blog);
		return $tpl;
	}

	public function getHtml($blog) {
		ob_start();
		include 'templates/BlogWidgetView.php';
		return ob_get_clean();
	}

}