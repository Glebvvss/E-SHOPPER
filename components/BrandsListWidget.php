<?php 

namespace app\components;

use Yii;
use Yii\base\Widget;
use yii\db\Query;

class BrandsListWidget extends Widget {

	public $brands;

	public function run() {
		$this->countProductsInBrands();
		$tpl = $this->getHtml($this->brands);
		return $tpl;
	}

	private function getHtml($brands) {		
		ob_start();
		include 'templates/BrandsListWidgetView.php';
		return ob_get_clean();
	}

	private function countProductsInBrands() {
		$query = new Query();
		$i = 0;
		foreach ($this->brands as $brand) {
			$count = $query->select('COUNT(*) AS count')
				  ->from('product')
				  ->where(['id_brand' => $brand['id_brand']])
				  	  ->createCommand()->queryAll();

			$this->brands[$i]['count'] = $count[0]['count'];
			$i++;
		}
	}

}