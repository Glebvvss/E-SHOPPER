<?php 

namespace app\components;

use Yii;
use Yii\base\Widget;
use app\models\Product;

class CategoryItemsWidget extends Widget {

	public $items;
	public $category;

	public function run() {
		$tpl = $this->getHtml($this->items, $this->category);
		return $tpl;
	}

	public function getHtml($items, $category) {
		ob_start();
		include 'templates/CategoryItemsWidgetView.php';
		return ob_get_clean();
	}

}