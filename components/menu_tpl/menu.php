<h2>Category</h2>
<div class="panel-group category-products" id="accordian">	

	<?php foreach ($tree as $category) : ?>
    <?php $id_name = strtolower($category['name_category']); ?>
	<?php if(isset( $category['childs'] )) : ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="#<?=$id_name?>">
                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                        <a href="<?=Yii::$app->urlManager->createUrl(['site/category', 'id_category' => $category['id_category'], 'category' => $category['name_category']])?>"><b><?=$category['name_category']?></b></a>
                    </a>
                </h4>
            </div>
            <div id="<?=$id_name?>" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul>
                    	<?php foreach($category['childs'] as $subCategory) : ?>	  
                        	<li><a href="<?=Yii::$app->urlManager->createUrl(['site/category', 'id_category' => $subCategory['id_category'], 'category' => $subCategory['name_category']])?>"><?=$subCategory['name_category']?></a></li>
                   		<?php endforeach; ?>

                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>

	<?php if(!isset($category['childs'])) : ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">

                    <a href="<?=Yii::$app->urlManager->createUrl(['site/category', 'id' => $category['id_category'], 'category' => $category['name_category']])?>"><b><?=$category['name_category']?></b></a>
                </h4>
            </div>
        </div>
		<?php endif; ?>
    <?php endforeach; ?>
</div>