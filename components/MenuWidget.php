<?php

namespace app\components;

use yii;
use yii\base\Widget;
use app\models\Category;

class MenuWidget extends Widget {
	
	public $tpl;
	private $data;
	private $tree;
	private $menuHtml;

	public function init() {
		parent::init();
		if ( $this->tpl === null ) 
			$this->tpl = 'menu';
		$this->tpl .= '.php';
	}

	public function run() {
		//get cache
		//$menu = Yii::$app->cache->get('menu');
		if($menu) return $menu;
		$this->data = Category::find()->asArray()->indexBy('id_category')->all();
		$this->tree = $this->getTree();
		$this->menuHtml = $this->getMenuHtml($this->tree);
		//set cache
		//Yii::$app->cache->set('menu', $this->menuHtml, 0);
		return $this->menuHtml;
	}

	//возвращает массив по принципы структуры дерева
	private function getTree() {
		$tree = [];
		foreach ($this->data as $id => &$node) {
			if (!$node['id_parent'])
				$tree[$id] = &$node;
			else
				$this->data[$node['id_parent']]  ['childs'][$node['id_category']] = &$node;
		}return $tree;
	}

	private function getMenuHtml($tree) {	
		ob_start();
		include __DIR__ . '/menu_tpl/' . $this->tpl;
		return ob_get_clean();
	}
}