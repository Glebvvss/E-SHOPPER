<?php

namespace app\components;

use yii;
use yii\base\Widget;

class ItemWidget extends Widget {

	public $item;
	public $sizes;
	public $form;
	public $reviews;
	public $count;

	public function run() {
		$tpl = $this->getHtml($this->item, $this->sizes, $this->form, $this->reviews, $this->count);
		return $tpl;
	}

	public function getHtml($item, $sizes, $form, $reviews, $count) {
		ob_start();
		include 'templates/ItemWidgetView.php';
		return ob_get_clean();
	}

}