<?php

namespace app\components;

use yii;
use yii\base\Widget;

class CommentWidget extends Widget {

	public $data;

	public function run() {
		$tree = $this->getTree();
		//debug($tree);
		//debug($tree);
		$tpl = $this->getHtml($tree);
		return $tpl;
	}

	private function getHtml($tree) {
		ob_start();
		include 'templates/CommentWidgetView.php';
		return ob_get_clean();
	}

	public function getTree() {
		$tree = [];
		foreach ($this->data as $id => &$node) {
			if (!$node['id_parent'])
				$tree[$id] = &$node;
			else
				$this->data[$node['id_parent']]  ['child'][$node['id_comment']] = &$node;
		}return $tree;
	}
}