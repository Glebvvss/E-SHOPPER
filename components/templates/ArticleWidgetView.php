<div class="blog-post-area article" id="article" data-id="ar-<?=$id_article?>">
	<h2 class="title text-center">Article of Blog</h2>
	<div class="single-blog-post">
		<h3><?=$article['article']['caption']?></h3>
		<div class="post-meta">
			<ul>
				<li><i class="fa fa-user"></i> <?=$article['article']['user']['username']?></li>
				<li><i class="fa fa-clock-o"></i> <?=  Yii::$app->formatter->asTime($article['article']['time_blog'], 'short')  ?></li>
				<li><i class="fa fa-calendar"></i> <?=  Yii::$app->formatter->asDate($article['article']['date_blog'], 'medium')  ?></li>
			</ul>
		</div>
		<a href=""><img src="/web/images/blog/<?=$article['article']['img_blog']?>" alt=""></a>
		<p><?=nl2br($article['article']['article'])?></p>

		<div class="pager-area">
			<ul class="pager pull-right">
				<?php if($article['last_article']['id_article'] != null) : ?>
					<li><a href="<?=yii::$app->urlManager->createUrl(['blog/article', 'id_article' => $article['last_article']['id_article']])?>">Pre</a></li>
				<?php endif; ?>
				<?php if($article['next_article']['id_article'] != null) : ?>
					<li><a href="<?=yii::$app->urlManager->createUrl(['blog/article', 'id_article' => $article['next_article']['id_article']])?>">Next</a></li>
				<?php endif; ?>
			</ul>
		</div>

	</div>
</div><!--/blog-post-area-->

<div class="rating-area">
	<ul class="tag">
		<li>TAG:</li>
		<?php $i = 0 ?>
		<?php foreach($tags as $tag) : ?>
			<li><a class="color" href="<?=yii::$app->urlManager->createUrl([ 'blog/blog', 'tag' => $tag['tag']['tag'] ])?>"> <?php if ($i != 0) : ?><span>/</span><?php endif; $i++?> <?=$tag['tag']['tag']?>  </a></li>
		<?php endforeach; ?>
	</ul>
</div><!--/rating-area-->

<div class="socials-share">
	<a href=""><img src="/web/images/blog/socials.png" alt=""></a>
</div><!--/socials-share-->

<div class="media commnets">
	<a class="pull-left" href="#">
		<img class="user-img" src="/web/images/users/<?=$article['article']['user']['img_user']?>" alt="">
	</a>
	<div class="media-body">
		<h4 class="media-heading"><?=$article['article']['user']['user_name']?></h4>
		<p><?=$article['article']['user']['about_user']?></p>
		<div class="blog-socials">
			<ul>
				<li><a href=""><i class="fa fa-facebook"></i></a></li>
				<li><a href=""><i class="fa fa-twitter"></i></a></li>
				<li><a href=""><i class="fa fa-dribbble"></i></a></li>
				<li><a href=""><i class="fa fa-google-plus"></i></a></li>
			</ul>
			<a class="btn btn-primary" href="<?=yii::$app->urlManager->createUrl([ 'blog/blog', 'author_posts_id' => $article['article']['user']['id_user'] ])?>">Other Posts</a>
		</div>
	</div>
</div><!--Comments-->


