<?php if(isset($hits)) : ?>

    <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Popular Items</h2>
        <?php foreach( $hits as $hit ) : ?>                            

            <div class="col-sm-4">
                <div class="product-image-wrapper">
                    <div class="single-products">
                        <div class="productinfo text-center">
                            <a href="<?=Yii::$app->urlManager->createUrl(["site/item", 'id' => $hit->id_product])?>">
                                <img src="/web/images/products/<?=$hit['img_product']?>" alt="" />
                            </a>
                            <h2>$<?=$hit->price?></h2>
                            <p><a class="item-link" href="<?=Yii::$app->urlManager->createUrl(["shop/item", 'id' => $hit->id_product])?>"><?=$hit->name_product?></a></p>
                            <a href="<?=yii::$app->urlManager->createUrl(['cart/add', 'id' => $hit->id_product])?>" data-id="<?= $hit->id_product ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                        </div>

                        <?php if($hit->sale == '1') : ?>
                        <img src="/web/images/home/sale.png" class="new" alt="" />
                        <?php endif; ?>
                        <?php if($hit->new == '1') : ?>
                        <img src="/web/images/home/new.png" class="new" alt="" />
                        <?php endif; ?>

                    </div>
                    <div class="choose">

                        <ul class="nav nav-pills nav-justified">
                            <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                    <!--        <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>-->
                        </ul>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>   
    </div><!--features_items-->

<?php endif; ?>