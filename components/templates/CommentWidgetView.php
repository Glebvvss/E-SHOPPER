<div id="comments">
	<?php function rec($nodes) { ?>
		<?php foreach($nodes as $node) : ?>
		<li class="media-response">		
			<div>
			<a class="img-left" href="#">
				<img class="user-img" src="/web/images/users/<?=$node['user']['img_user']?>">
			</a>
			<div class="media-text-response">
				<ul class="sinlge-post-meta">
					<li><i class="fa fa-user"></i><?=$node['user']['username']?></li>
					<li><i class="fa fa-clock-o"></i> <?= Yii::$app->formatter->asTime($node['time_comment'], 'short') ?></li>
					<li><i class="fa fa-calendar"></i> <?= Yii::$app->formatter->asDate($node['date_comment'], 'medium') ?></li>
				</ul>

				<div class="comment-block">
					<p class="comment-text"><?=nl2br($node['comment'])?></p>

					<div class="replay-button"> 			
						<button class="btn btn-primary show-replay" id="show-<?=$node['id_comment']?>"><i class="fa fa-reply"></i>Replay</button>
					</div>
				</div>		
			
				<div class="replay" id="com-<?=$node['id_comment']?>">
					<div class="left-block"></div>
					<div><textarea id="text-<?=$node['id_comment']?>"></textarea></div>
					<div class="submit-comment-div"><button class="btn btn-primary submit-comment" id="submit-<?=$node['id_comment']?>">Submit</button></div>
				</div>
			</div>

			<ul>
				<?php 	if($node['child']) {
							rec($node['child']);
						}
				?>	
			</ul>
			</div>
		</li>;
		<?php endforeach; ?>
	<?php } ?>

	<div class="response-area">
		<h2>COMMENTS:</h2>
		<ul class="media-list">
			<?php rec($tree) ?>
		</ul>		
	</div>

	<div class="replay-box">
		<div class="row">					
			<h2>Leave a replay</h2>													
			<div class="text-area">
				<div class="blank-arrow"><label>Comment</label></div>
				<span>*</span>
				<textarea id="text-0"></textarea>
				<button class="btn btn-primary submit-comment" id="submit-0">post comment</button>
			</div>					
		</div>
	</div>
</div>