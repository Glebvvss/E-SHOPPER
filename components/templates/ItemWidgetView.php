<?php			
	use yii\widgets\ActiveForm;
	use yii\helpers\Html; 
?>

<div class="product-details"><!--product-details-->
	<div class="col-sm-5">
		<div class="view-product">
			<img src="/web/images/products/<?=$item['img_product']?>" alt="" />
		</div>

		<div id="similar-product" class="carousel slide" data-ride="carousel">
			  <!-- Wrapper for slides -->
			    <div class="carousel-inner">

					<div class="item active">
					  <a href=""><img src="/web/images/product-details/similar1.jpg" alt=""></a>
					  <a href=""><img src="/web/images/product-details/similar2.jpg" alt=""></a>
					  <a href=""><img src="/web/images/product-details/similar3.jpg" alt=""></a>
					</div>

					<div class="item">
					  <a href=""><img src="/web/images/product-details/similar3.jpg" alt=""></a>
					  <a href=""><img src="/web/images/product-details/similar2.jpg" alt=""></a>
					  <a href=""><img src="/web/images/product-details/similar1.jpg" alt=""></a>
					</div>
					
					<div class="item">
					  <a href=""><img src="/web/images/product-details/similar1.jpg" alt=""></a>
					  <a href=""><img src="/web/images/product-details/similar2.jpg" alt=""></a>
					  <a href=""><img src="/web/images/product-details/similar3.jpg" alt=""></a>
					</div>

				</div>

			  <!-- Controls -->
			  <a class="left item-control" href="#similar-product" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			  </a>
			  <a class="right item-control" href="#similar-product" data-slide="next">
				<i class="fa fa-angle-right"></i>
			  </a>
		</div>

	</div>
	
	<div class="col-sm-7">
		<div class="product-information"><!--/product-information-->
			<?php if ($item['new'] === 1) : ?>
			<img src="/web/images/product-details/new.jpg" class="newarrival" alt="" />
			<?php endif; ?>

			<h2><?=$item['name']?></h2>
			<p>Web ID: <?=$item['id_product']?></p>
			<img src="/web/images/product-details/rating.png" alt="" />
			<span>
				<span>US $<?=$item['price']?></span>
				<label>Quantity:</label>
				<input type="text" value="3" />
				<button type="button" class="btn btn-fefault cart">
					<i class="fa fa-shopping-cart"></i>
					Add to cart
				</button>
			</span>

			<?php 
				if ($item['new'] === 1) 
					$condicion = 'New';
				elseif ($item['sale'] === 1)
					$condicion = 'Sale';
				else 
					$condicion = 'Default';
			?>

			<p><b>Condition: </b><?=$condicion?></p>
			<?php $brand = $item['brand']; ?>
			<p><b>Brand: </b><?=$item['name_brand']?></p>
			<a href=""><img src="/web/images/product-details/share.png" class="share img-responsive"  alt="" /></a>
		</div><!--/product-information-->
	</div>

</div><!--/product-details-->

<div class="category-tab shop-details-tab"><!--category-tab-->
	<div class="col-sm-12">
		<ul class="nav nav-tabs">
			<li><a href="#details" data-toggle="tab">Details</a></li>
			<li><a href="#companyprofile" data-toggle="tab">Company Profile</a></li>
			<li><a href="#tag" data-toggle="tab">Tag</a></li>
			<li class="active"><a href="#reviews" data-toggle="tab">Reviews (<?=$count?>)</a></li>
		</ul>
	</div>
	
	<div class="tab-content"><!--about items-->

		<div class="tab-pane fade" id="details">
			<div>
				<p>About item: <?=$item['description']?></p>
				<p>
					Sizes: 
					<?php foreach($item['sizes'] as $size) : ?>
						<?=$size['name_size']?>
					<?php endforeach; ?>
				</p>
				<p>Material: <?=$item['name_material']?></p>
				<p>Country: <?=$item['country']?></p>
			</div>
		</div>

		<div class="tab-pane fade" id="companyprofile">
			<div>
				<?=$item['about_brand']?>
			</div>
		</div>

		<div class="tab-pane fade" id="tag">
			<p>			
				<?php foreach($item['tags'] as $tag) : ?>					
					<a class="color-tag" href="<?= Yii::$app->urlManager->createUrl(['shop/category', 'tag' => $tag['tag']]) ?>">
						<?=$tag['tag']?>
					</a>
				<?php endforeach; ?>			
			</p>
		</div>
	
		<div class="tab-pane fade active in" id="reviews"><!--reviews-->
			<?php foreach($reviews as $review) : ?>
				<div class="col-sm-12">
					<ul>
						<li><a href=""><i class="fa fa-user"></i><?=  $review['username']  ?></a></li>
						<li><a href=""><i class="fa fa-clock-o"></i><?=  $review['time_review']  ?></a></li>
						<li><a href=""><i class="fa fa-calendar-o"></i><?=  $review['date_review']  ?></a></li>
					</ul>
					<p><?=$review['review']?></p>
				</div>
			<?php endforeach; ?>
			<div class="review-form">

			<?php if ( !Yii::$app->user->isGuest ) : ?>
				<p><b>Write Your Review</b></p>

				<?php $f = ActiveForm::begin() ?>				
					<?= $f->field($form, 'review')->textarea(['rows' => '11'])->label(false) ?>
					<b>Rating: </b> <img src="/web/images/product-details/rating.png" alt="" />
					<button type="submit" class="btn btn-default pull-right">
						Submit
					</button>								
				<?php ActiveForm::end() ?>
			<?php endif; ?>				

			</div>			
		</div>	<!--/reviews-->
	</div><!--/about items-->
</div><!--/category-tab-->