<h2 class="title text-center">Blog</h2>

<?php foreach ($blog as $article) : ?>

<div class="single-blog-post">
	<h3><?=$article['caption']?></h3>
	<div class="post-meta">
		<ul>
			<li><i class="fa fa-user"></i><?=$article['username']?></li>
			<li><i class="fa fa-clock-o"></i><?=Yii::$app->formatter->asTime($article['time_blog'], 'short')?></li>
			<li><i class="fa fa-calendar"></i><?= Yii::$app->formatter->asDate($article['date_blog'], 'medium') ?></li>
		</ul>
	</div>

	<a href="<?=yii::$app->urlManager->createUrl(['blog/article', 'id_article' => $article['id_article']])?>">
		<img src="/web/images/blog/<?=$article['img_blog']?>" alt="">
	</a>

	<p><?=$article['article_short']?></p>
	<a  class="btn btn-primary" href="<?=yii::$app->urlManager->createUrl(['blog/article', 'id_article' => $article['id_article']])?>">Read More</a>
</div>

<?php endforeach; ?>