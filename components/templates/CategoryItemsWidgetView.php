<?php if ($category) : ?>
	<h2 class="title text-center"><?=$category?></h2>
<?php endif; ?>

<?php if (!$category) : ?>
	<h2 class="title text-center">Popular Items</h2>
<?php endif; ?>

<?php if(count($items) === 0) : ?>
	<p>Sorry, but this category is empty.</p>
<?php endif; ?>

<?php $i = 0; foreach( $items as $item ) : ?>
	<div class="col-sm-4">
		<div class="product-image-wrapper">
			<div class="single-products">
				<div class="productinfo text-center">
					<a href="<?=Yii::$app->urlManager->createUrl(['shop/item', 'id' => $item['id_product']])?>"><img src="/web/images/products/<?=$item['img_product']?>" alt="" /></a>
					<h2>$<?=$item['price']?></h2>
					<a href="<?=Yii::$app->urlManager->createUrl('shop/item')?>"><p><?=$item['name_product']?></p></a><a href="<?=yii::$app->urlManager->createUrl(['cart/add', 'id' => $item['id_product']])?>" data-id="<?= $item['id_product'] ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
				</div>

		        <?php if($item['sale'] == '1') : ?>
                <img src="/web/images/home/sale.png" class="new" alt="" />
                <?php endif; ?>
                <?php if($item['new'] == '1') : ?>
                <img src="/web/images/home/new.png" class="new" alt="" />
                <?php endif; ?>

			</div>
			<div class="choose">
				<ul class="nav nav-pills nav-justified">
					<li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
					<!--<li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>-->
				</ul>
			</div>
		</div>
	</div>
	<?php $i++; if ($i % 3 === 0) : ?>
		<div class="clearfix"></div>
	<?php endif; ?>
<?php endforeach; ?>
<div class="clearfix"></div>