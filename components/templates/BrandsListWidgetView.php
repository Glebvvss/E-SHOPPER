<div class="brands_products"><!--brands_products-->
    <h2>Brands</h2>
    <div class="brands-name">
        <ul class="nav nav-pills nav-stacked">
            <?php foreach($brands as $brand) : ?>
            	<li>
            		<a href="<?= Yii::$app->urlManager->createUrl(['shop/category', 'brand' => $brand->name_brand]) ?>"> 
            			<span class="pull-right">(<?=$brand['count']?>)</span><?=$brand['name_brand']?>
            		</a>
            	</li>
            <?php endforeach; ?>
        </ul>
    </div>
</div><!--/brands_products-->