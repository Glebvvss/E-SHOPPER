<?php 

namespace app\components;

use Yii;
use Yii\base\Widget;
use app\models\Product;

class FeaturesItemsWidget extends Widget {

	public $hits;

	public function run() {
		$tpl = $this->getHtml($this->hits);
		return $tpl;
	}

	private function getHtml($hits) {
		ob_start();
		include 'templates/FeaturesItemsWidgetView.php';
		return ob_get_clean();
	}

}