<?php

namespace app\models;

use yii\db\ActiveRecord;

class Tag extends ActiveRecord{

	public static function tableName() {
		return 'tag';
	}

	public function getTagBlog() {
		return $this->hasMany(TagBlog::className(), ['id_tag' => 'id_tag']);
	}
	
}