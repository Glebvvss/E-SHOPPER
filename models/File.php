<?php

namespace app\models;

use Yii;
use \yii\helpers\FileHelper;

class File {

	public $id_user;

	public function __construct($id_user = null) {
		$this->id_user = $id_user;
	}

	public function addAvatarFileFromBuffer() {
		$fileInBuffer = $_FILES['avatar']['tmp_name'];
		if ($fileInBuffer)
			$this->addAvatar();
	}

	private function addAvatar() {
		$this->deleteOldAvatarFromDir();
		$img_user = $this->createAvatarName();
		$this->addAvatarNameToDatabase($img_user);		
		copy($_FILES['avatar']['tmp_name'], "images/users/" . $img_user);
	}

	private function deleteOldAvatarFromDir() {
		$files = scandir('images/users');
		foreach ($files as $file) {
			$fileArr = explode('.', $file);
			if ($fileArr[0] == 'user' . $this->id_user) {
				unlink('images/users/' . $file);
				break;
			}
		}
	}

	private function addAvatarNameToDatabase($img_user) {
		$arr['img_user'] = $img_user;
		Yii::$app->db->createCommand()->update( 'user', $arr, ['id_user' => $this->id_user])->execute(); 
	}
	
	private function createAvatarName() {
		$file = explode('.', $_FILES['avatar']['name']);
		$extention = $file[1];
		return 'user'.$this->id_user.".".$extention;
	}
}