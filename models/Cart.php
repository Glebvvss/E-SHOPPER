<?php 

namespace app\models;
use yii\base\ActiveRecord;

class Cart {

	public function addToCart($product, $qty = 1) {
		if (isset($_SESSION['cart'][$product->id_product])) {
			$_SESSION['cart'][$product->id_product]['qty'] += $qty;
		}else{
			$_SESSION['cart'][$product->id_product] = [
				'name_product' => $product->name_product,
				'img_product' => $product->img_product,
				'price' => $product->price,
				'id_product' => $product->id_product,
				'qty' => $qty
			];
		}
		//qty in cart
		$_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? 
		$_SESSION['cart.qty'] += $qty : $qty;
		//sum of cart
		$_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? 
		$_SESSION['cart.sum'] += $qty * $product->price : $qty * $product->price;

	}

	public function deleteFromCart($id) {
		//new sum of cart
		$_SESSION['cart.sum'] -= $_SESSION['cart'][$id]['price'] * $_SESSION['cart'][$id]['qty'];
		//new qty of cart
		$_SESSION['cart.qty'] -= $_SESSION['cart'][$id]['qty'];
		unset($_SESSION['cart'][$id]);
	}

	public function redactQty($id, $new_qty) {
		$old_qty = $_SESSION['cart'][$id]['qty'];
		//new sum of cart
		$_SESSION['cart.sum'] -= $_SESSION['cart'][$id]['price'] * $old_qty;
		$_SESSION['cart.sum'] += $_SESSION['cart'][$id]['price'] * $new_qty;
		//new qty of cart
		$_SESSION['cart.qty'] -= $old_qty;
		$_SESSION['cart.qty'] += $new_qty;
		//new qty of item
		$_SESSION['cart'][$id]['qty'] = $new_qty;
	}

}