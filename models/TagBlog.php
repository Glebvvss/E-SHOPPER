<?php

namespace app\models;

use yii\db\ActiveRecord;

class TagBlog extends ActiveRecord{

	public static function tableName() {
		return 'tag_blog';
	}

	public function getTag() {
		return $this->hasOne(Tag::className(), ['id_tag' => 'id_tag']);
	}
	
	public function getBlog() {
		return $this->hasOne(Blog::className(), ['id_blog' => 'id_blog'])->viaTable('user', ['id_user' => 'id_user']);;
	}

	public static function arrTags($id_blog) {
		$tags = self::find()->asArray()->joinWith('tag')->where(['id_blog' => $id_blog])->all();
		return $tags;
	}

	public function arrBlogs($tag) {
		$id_tag = Tag::find()->select('id_tag')->asArray()->where(['tag' => $tag])->one();
		$blogs = self::find()->asArray()->join('NATURAL JOIN', 'tag', 'tag_blog.id_tag = tag.id_tag')->all();
		return $blogs;
	}
}