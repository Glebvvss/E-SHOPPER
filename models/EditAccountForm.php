<?php

namespace app\models;


class EditAccountForm extends \yii\base\Model {

	public $name;
	public $middle_name;
	public $last_name;
	public $display_name;
	public $email;
	public $country;
	public $city;
	public $street;
	public $house;
	public $address;
	public $postal_code;
	public $phone;
	public $mobile_phone;
	public $img_user;
	public $apartment;

}