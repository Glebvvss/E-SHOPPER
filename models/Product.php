<?php

namespace app\models;

use Yii;
use app\models\Category;
use app\models\User;
use yii\db\ActiveRecord;
use yii\db\Query;


class Product extends ActiveRecord {

	public static function tableName() {
		return 'product';
	}

	public function getBrand() {
		return $this->hasOne(Brand::className(), ['id_brand' => 'id_brand']);
	}

	public function getCategory() {
		return $this->hasOne(Category::className(), ['id_category' => 'id_category']);
	}

	public function itemInformaitionOutput($id_product) {
		$query = new Query();
		$item = $query->select('*')->from('product')
			->join('NATURAL JOIN', 'brand')
			->join('NATURAL JOIN', 'material')			
			->join('NATURAL JOIN', 'category')
			->where(['id_product' => $id_product])
				->createCommand()->queryOne();				

		$item['sizes'] = $this->sizesOfItem($id_product);
		$item['tags'] = $this->tagsOfItem($id_product);
		
		return $item;
	}

	private function sizesOfItem($id_product) {
		$query = new Query();
		return $query->select('name_size')
					 ->from('size')
					 ->join('NATURAL JOIN', 'size_product')
			 		 ->where(['id_product' => $id_product])
			 		 ->createCommand()
			 		 ->queryAll();
	}

	private function tagsOfItem($id_product) {
		$query = new Query();
		return $query->select('*')
					 ->from('tag')
					 ->join('NATURAL JOIN', 'tag_item')
					 ->where(['id_product' => $id_product])
					 ->createCommand()
			 		 ->queryAll();	
	}

	public function itemReviewsOutput($id_product) {
		$query = new Query();
		return $query->select('*')
					 ->from('review_item')			
					 ->join('NATURAL JOIN', 'user')
					 ->where(['id_product' => $id_product])
					 ->createCommand()
					 ->queryAll();
	}

	public function countOfReviews($id_product) {
		$query = new Query();
		$cnt = $query->select('COUNT(*) AS cnt')
		  			 ->from('review_item')			
					 ->join('NATURAL JOIN', 'user')
					 ->where(['id_product' => $id_product])
					 ->createCommand()
					 ->queryAll();
		return $cnt[0]['cnt'];
	}

	//output products block
	public function outputProducts($brand = null) {
		$id_category = Yii::$app->request->get('id_category');
		$tag = Yii::$app->request->get('tag');		

		if ($brand) 
			return $this->getProductsOfBrand($brand);

		if ($tag)
			return $this->getProductsOfTag($tag);

		if (!$id_category) 
			return self::getAllProducts();
	
		$categoryInfo = Category::find()->where(['id_category' => $id_category])->one();				
		if ($categoryInfo['id_parent'] === 0)
			return $this->getProductsFormMajorCategory($id_category);

		return $this->getProductsFromSubcategory($id_category);
	}

	private static function getAllProducts() {
		return self::find()->orderBy(['id_product' => SORT_DESC]);
	}

	private function getProductsOfTag($tag) {
		$query = new Query();
		return $query->select('*')->from('tag_item')
					 ->leftJoin('tag', 'tag.id_tag = tag_item.id_tag')
					 ->leftJoin('product', 'product.id_product = tag_item.id_product')
					 ->where(['tag' => $tag]);
					 //->orderBy(['id_product' => SORT_DESC]);
	}

	private function getProductsFormMajorCategory($id_category) {
		$query = new Query();
		return $query->select('*')
					 ->from('product')
					 ->join('NATURAL JOIN', 'category')
					 ->where(['id_parent' => $id_category])
					 ->orderBy(['id_product' => SORT_DESC]);;
	}

	private function getProductsFromSubcategory($id_category) {
		$query = new Query();
		return $query->select('*')
					 ->from('product')
					 ->where(['id_category' => $id_category])
					 ->orderBy(['id_product' => SORT_DESC]);;	
	}

	private function getProductsOfBrand($brand) {
		$query = new Query();
		return $query->select('*')
					 ->from('product')
					 ->join('NATURAL JOIN', 'brand')
					 ->where(['name_brand' => $brand])
					 ->orderBy(['id_product' => SORT_DESC]);;
	}
	//end of output products block

	public function addReview($id_product, $reviewShopForm = null) {
		if ( Yii::$app->user->isGuest || $reviewShopForm == null ) return; 		
		$arr['id_review'] = null;
		$arr['id_user'] = Yii::$app->user->getId();
		$arr['review'] = $reviewShopForm['review'];
		$arr['id_product'] = $id_product;
		$arr['time_review'] = date('H:i:s');
		$arr['date_review'] = date('Y-m-d');

		Yii::$app->db->createCommand()->insert('review_item', $arr)->execute();
	}





	private function getArrWithProductsInCategories() {
		$products = self::find()->asArray()->all();
		$categories = Category::find()->indexBy('id_category')->asArray()->all();

		foreach ($products as $product) {
			$categories[ $product['id_category'] ]['products'][] = $product;
		}
		return $categories;
	}

	public function allProductsToMajorCategory() {

		$categories = $this->getArrWithProductsInCategories();
		
		$productsOfMajorCategories = [];
		foreach ($categories as $id => &$category) {
			if (!$category['id_parent'])
				$productsOfMajorCategories[$id] = &$category;
			else {

				$count = count($category['products']);
				$i = 0;
				while ($i < $count) {
					$categories[$category['id_parent']]  ['products'][] = &$category['products'][$i];
					$i++;
				}

			}
		}return $productsOfMajorCategories;
	}







	
}








