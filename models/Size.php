<?php

namespace app\models;

use yii\db\ActiveRecord;

class Size extends ActiveRecord{

	public static function tableName() {
		return 'size';
	}

	public function getSizeProduct() {
		return $this->hasMany(SizeProduct::className(), ['id_size' => 'id_size']);
	}
	
}