<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\User;

class Blog extends ActiveRecord {

	public static function tableName() {
		return 'blog';
	}

	public function getUser() {
		return $this->hasOne(User::className(), ['id_user' => 'id_user']);
	}

	public function getTagBlog() {
		return $this->hasMany(TagBlog::className(), ['id_blog' => 'id_blog']);
	}

	//block output article
	public function outputBlogArticles($tag = null) {
		$author_posts_id = yii::$app->request->get('author_posts_id');
		
		if($author_posts_id) 
			return $this->getArticlesOfAuthor($author_posts_id);
		if ($tag) 
			return $this->getArticlesWithTag($tag);					
		else 
			return $this->getAllArticles();		
	}

	private function getArticlesOfAuthor($author_posts_id) {
		$query = new Query();
		return $query->select('*')
					 ->from('blog')
					 ->join('NATURAL JOIN', 'user')
					 ->orderBy(['id_article' => SORT_DESC]);		
	}

	private function getArticlesWithTag($tag) {
		$query = new Query();
		return $query->select('*')->from('tag_blog')
					 ->leftJoin('tag', 'tag.id_tag = tag_blog.id_tag')
					 ->leftJoin('blog', 'blog.id_article = tag_blog.id_blog')
					 ->leftJoin('user', 'user.id_user = blog.id_user')
					 ->where(['tag' => $tag])
					 ->orderBy(['id_article' => SORT_DESC]);
	}

	private function getAllArticles() {
		$query = new Query();
		return $query->select('*')
					 ->from('blog')
					 ->join('NATURAL JOIN', 'user')
					 ->orderBy(['id_article' => SORT_DESC]);
	}
	//end of block output article
	
	public static function getLastIdArticle() {		
		return self::find()->max('id_article');
	}






	//get anyone article with your neighbor articles
	public static function lastNextArticles($id_article) {		
		$all_articles = self::find()->asArray()->joinWith('user')->orderBy(['id_article' => SORT_DESC])->all();

		$i = 0;
		$all = count($all_articles);//quantity articles in blog
		while ($all_articles[$i]['id_article'] != $id_article || $all_articles[$i] < $all) $i++;

		if ($all == $i - 1)
			return;//article is not exist

		$lastNextArticles = [
			'article' => $all_articles[$i],
			'next_article' => $all_articles[$i - 1],
			'last_article' => $all_articles[$i + 1]
		];
		return $lastNextArticles;
	}

}