<?php 

namespace app\models;

class SingUpForm extends \yii\base\Model {

	public $username;
	public $password;
	public $confirm_password;

	public function rules() {
		return [
			[['username', 'password', 'confirm_password'], 'required'],
			['confirm_password', 'passwordMatch']
		];
	}

	public function passwordMatch ( $attribute ) {
    	if ( $this->password !== $this->confirm_password )
    	        $this->addError ( $attribute, "Пароли не совпадают" );
	}


}