<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

class Comment extends ActiveRecord {

	public static function tableName() {
		return 'comment';
	}

	public function getUser() {
		return $this->hasOne(User::className(), ['id_user' => 'id_user']);;
	}

	public static function getComments($id_parent, $id_article, $comment) {
		self::addCommentToBlog($id_parent, $id_article, $comment);
		return self::find()->asArray()
						   ->joinWith('user')
						   ->where(['id_article' => $id_article])
						   ->indexBy('id_comment')
						   ->all();
	}

	private static function addCommentToBlog($id_parent, $id_article, $comment) {
		if( $id_parent === null || $comment === null) return;

		$arr['id_comment'] = null;
		$arr['id_parent'] = $id_parent;
		$arr['id_article'] = $id_article;
		$arr['id_user'] = Yii::$app->user->getId();;
		$arr['comment'] = $comment;
		$arr['date_comment'] = date('Y-m-d');
		$arr['time_comment'] = date('H:i:s');

		Yii::$app->db->createCommand()->insert('comment', $arr)->execute();
	}

}