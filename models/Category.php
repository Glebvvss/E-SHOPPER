<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\models\Product;

class Category extends ActiveRecord{

	public static function tableName() {
		return 'category';
	}

	public function getBrand() {
		return $this->hasMany(Brand::className(), ['id_category' => 'id_category']);
	}
	
}
