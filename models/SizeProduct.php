<?php

namespace app\models;

use yii\db\ActiveRecord;

class SizeProduct extends ActiveRecord{

	public static function tableName() {
		return 'size_product';
	}

	public function getSize() {
		return $this->hasOne(Size::className(), ['id_size' => 'id_size']);
	}
	
}