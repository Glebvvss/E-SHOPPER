<?php

namespace app\models;

use yii\db\ActiveRecord;

class Brand extends ActiveRecord{

	public static function tableName() {
		return 'brand';
	}

	public function getProduct() {
		return $this->hasMany(Product::className(), ['id_product' => 'id_product']);
	}
	
}