<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "user".
 *
 * @property integer $id_user
 * @property string $username
 * @property string $img_user
 * @property string $about_user
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 *
 * @property Blog[] $blogs
 * @property Comment[] $comments
 * @property ReviewItem[] $reviewItems
 */
class User extends ActiveRecord
{

    private $username;
    private $password;
    private $confirm_password;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'img_user', 'about_user', 'password', 'authKey', 'accessToken'], 'required'],
            [['about_user'], 'string'],
            [['username'], 'string', 'max' => 100],
            [['img_user'], 'string', 'max' => 200],
            [['password', 'authKey', 'accessToken'], 'string', 'max' => 255],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogs()
    {
        return $this->hasMany(Blog::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewItems() {
        return $this->hasMany(ReviewItem::className(), ['id_user' => 'id_user']);
    }

    public static function editAccount($id_user) {
        $user = self::find()->where(['id_user' => $id_user])->one();
        return $user;
    }

    public function applyEditAccount($id_user, $post) {
        unset($post['img_user']);
        Yii::$app->db->createCommand()
            ->update( 'user', $post, ['id_user' => "$id_user"])->execute();        
    }

    public function addUser($request) {

        $this->postRequest($request);
        if ($this->validateUsername() === false)
            return 'Такой пользователь уже существует!';

        if ($this->validatePassword() === false)
            return 'Пароли не совпадают!';

        Yii::$app->db->createCommand("
            INSERT INTO `user`
                (`username`, `password`)
                    VALUES 
                        ('$this->username', '$this->password')
        ")->execute();

        return 'Регистрация прошла успешно';
    }

    private function postRequest($request) {
        $this->username = $request['username'];
        $this->password = $request['password'];
        $this->confirm_password = $request['confirm_password'];
    }

    private function validatePassword() {
        if ($this->password === $this->confirm_password) {
            return true;    
        }
        return false;
    }

    private function validateUsername() {
        if (self::find()->where(['username' => $this->username])->one() == null) {
            return true;
        }
        return false;
    }

    public function singUp() {
        $singUpFormRequest = Yii::$app->request->post('SingUpForm');
        if ($singUpFormRequest != null) {
            $singUpNote = $this->addUser($singUpFormRequest);
        }
        return $singUpNote;
    }



}




