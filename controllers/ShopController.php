<?php

namespace app\controllers;

use Yii;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\models\ReviewShopForm;
use app\models\SingUpForm;
use app\models\Category;
use app\models\Product;
use app\models\Brand;
use app\models\Blog;
use app\models\User;
use app\models\Comment;
use app\models\Cart;
use app\models\TagBlog;
use app\models\EditAccount;
use app\models\File;
use app\models\ResponseForm;
use app\models\SingUpModel;
use app\models\LoginForm;
use app\models\RegForm;
use app\models\Video;

use yii\data\Pagination;
use yii\db\Query;

class ShopController extends AppController {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


	public function actionCategory() {
		$brands = Brand::find()->asArray()->all();		
		$category = Yii::$app->request->get('category');
		$brandCategory = Yii::$app->request->get('brand');

		$product = new Product();
		$query = $product->outputProducts($brandCategory);

		$pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => '9', 'forcePageParam' => false, 'pageSizeParam' => false]);
		$items = $query->offset($pages->offset)->limit($pages->limit)->createCommand()->queryAll();
		return $this->render('category', [
			'category' => $category,
			'brands' => $brands,
			'items' => $items,
			'pages' => $pages			
		]);
	}


	public function actionItem() {
		$brands = Brand::find()->asArray()->all();
		$id_product = Yii::$app->request->get('id');
		$reviewShopForm = Yii::$app->request->post('ReviewShopForm');

		$product = new Product();
		$product->addReview($id_product, $reviewShopForm);
		$item = $product->itemInformaitionOutput($id_product);        
		$reviews = $product->itemReviewsOutput($id_product);
        $count = $product->countOfReviews($id_product);

		$form = new ReviewShopForm();
		return $this->render('item', [            
			'reviews' => $reviews,
			'brands' => $brands,
            'count' => $count,
			'item' => $item,
			'form' => $form
		]);
	}

}