<?php

namespace app\controllers;

use yii;
use yii\db\Query;
use app\models\Cart;
use app\models\Product;
use app\models\EditAccountForm;

class CartController extends AppController {

	public function actionOpen() {
		$session = yii::$app->session;
		$session->open();
		$this->layout = false;
		return $this->render('cart-modal', [
			'session' => $session
		]);
	}

	public function actionAdd() {
		$id = yii::$app->request->get('id');
		$product = Product::find()->where(['id_product' => $id])->one();

		if (empty($product)) return false;
		$session = yii::$app->session;
		$session->open();
		
		$cart = new Cart();
		$cart->addToCart($product);
		$this->layout = false;
		return $this->render('cart-modal', [
			'session' => $session
		]);
	}

	public function actionDelete() {
		$id = Yii::$app->request->get('id');
		
		if (empty($id)) return false;
		$session = yii::$app->session;
		$session->open();
		$cart = new Cart();

		$cart->deleteFromCart($id);
		$this->layout = false;
		return $this->render('cart-modal', [
			'session' => $session
		]);
	}

	public function actionQtyOfItem() {
		$id = Yii::$app->request->get('id');
		$new_qty = Yii::$app->request->get('qty');

		$session = yii::$app->session;
		$session->open();

		$cart = new Cart();
		$cart->redactQty($id, $new_qty);
		$this->layout = false;
		return $this->render('cart-modal', [
			'session' => $session
		]);
	}

	public function actionConfirmOrder() {
		$type_order = Yii::$app->request->get('type_order');
		$session = yii::$app->session;
		$session->open();
		$this->layout = false;
		return $this->render('send-order', [
			'session' => $session
		]);	
	}

}
