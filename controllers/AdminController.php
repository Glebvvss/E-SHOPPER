<?php

namespace app\controllers;

class AdminController extends AppController {

	public function actionAdmin() {
		return $this->render('admin');
	}

}