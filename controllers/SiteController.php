<?php

namespace app\controllers;

use Yii;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\models\EditAccountForm;
use app\models\ReviewShopForm;
use app\models\SingUpForm;
use app\models\Category;
use app\models\Product;
use app\models\Brand;
use app\models\Blog;
use app\models\User;
use app\models\Comment;
use app\models\Cart;
use app\models\TagBlog;
use app\models\EditAccount;
use app\models\File;
use app\models\ResponseForm;
use app\models\SingUpModel;
use app\models\LoginForm;
use app\models\RegForm;
use app\models\Video;

use yii\data\Pagination;
use yii\db\Query;

class SiteController extends AppController {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }



    public function actionLogin() {    	
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

		$user = new User();
        $singUpNote = $user->singUp();

        $loginFormModel = new LoginForm();
        if ($loginFormModel->load(Yii::$app->request->post()) && $loginFormModel->login()) {
            return $this->goBack();
        }

        $singUpFormModel = new SingUpForm();
        return $this->render('login', [
        	'singUpFormModel' => $singUpFormModel,
        	'loginFormModel' => $loginFormModel,
        	'singUpNote' => $singUpNote        
        ]);
    }


    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionAccount() {
    	$id_user = Yii::$app->session->get('__id');
    	$file = new File($id_user);
    	$user = User::find()->where(['id_user' => $id_user])->one();
    	return $this->render('account',[
    		'user' => $user
    	]);
    }

	public function actionEdit() {
		$id_user = Yii::$app->session->get('__id');
		$EditAccountFormRequest = yii::$app->request->post('EditAccountForm');

		$file = new File($id_user);
		$file->addAvatarFileFromBuffer();
        
		if ($EditAccountFormRequest) {
			User::applyEditAccount($id_user, $EditAccountFormRequest);
            $this->redirect('/site/account');
        }
        
		$user = User::editAccount($id_user);
		$form = new EditAccountForm();
    	return $this->render('edit', [
    		'form' => $form,
    		'user' => $user
    	]);
    }

	public function actionIndex() {
		$brands = Brand::find()->asArray()->all();
		$hits = Product::find()->where(['hit' => '1'])->limit(6)->all();
		$product = new Product();
		$productsInMajorCategories = $product->allProductsToMajorCategory();
		return $this->render('index', [
			'productsInMajorCategories' => $productsInMajorCategories,
			'brands' => $brands,
			'hits' => $hits
		]);
	}

	public function actionContact () {
		return $this->render('contact', [
			'circleOfHands' => $circleOfHands
		]);
	}

}