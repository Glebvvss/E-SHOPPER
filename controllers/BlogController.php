<?php

namespace app\controllers;

use Yii;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\models\Brand;
use app\models\Blog;
use app\models\User;
use app\models\Comment;
use app\models\Cart;
use app\models\TagBlog;
use app\models\WriteArticleForm;

use app\models\File;
use app\models\ResponseForm;

use yii\data\Pagination;
use yii\db\Query;

class BlogController extends AppController {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	public function actionBlog() {
		$brands = Brand::find()->asArray()->all();
		$tag = yii::$app->request->get('tag');

		$blog = new Blog();
		$blog = $query = $blog->outputBlogArticles($tag, $author_posts_id);        

		$pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => '5', 'forcePageParam' => false, 'pageSizeParam' => false]);
		$blog = $query->offset($pages->offset)->limit($pages->limit)->createCommand()->queryAll();
		return $this->render('blog', [
			'brands' => $brands,
			'pages' => $pages,
			'blog' => $blog
		]);
	}

	public function actionArticle() {
		$brands = Brand::find()->asArray()->all();
        $id_parent_comment = Yii::$app->request->post('id');        
        $text_comment = Yii::$app->request->post('text');
        $id_article = Yii::$app->request->get('id_article');
        
		if ($id_article == null)
			$id_article = Blog::getLastIdArticle();

        $blog = new Blog();
        $article = $blog->lastNextArticles($id_article);
        $comments = Comment::getComments($id_parent_comment, $id_article, $text_comment);        
		$tags = TagBlog::arrTags($id_article);
		return $this->render('article', [
            'id_article' => $id_article,
			'comments' => $comments,
			'article' => $article,
			'brands' => $brands,
			'tags' => $tags
		]);
	}

    public function actionUpdateCommentsInArticleAjax() {
        $id_article = Yii::$app->request->post('id_article');
        $id_parent_comment = Yii::$app->request->post('id');
        $text_comment = Yii::$app->request->post('text');

        $comments = Comment::getComments($id_parent_comment, $id_article, $text_comment);
        $this->layout = false;
        return $this->render('comments', [
            'comments' => $comments                   
        ]);    
    }

    public function actionWriteArticle() {
        $form = new WriteArticleForm();
        return $this->render('write-article', [
            'form' => $form
        ]);
    }

}