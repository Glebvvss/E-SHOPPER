<?php

namespace app\controllers;

class ThisController extends AppController {

	public function actionThis() {
		return $this->render('this');
	}

}