<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<section id="cart_items">
    <div class="container">

        <div class="user-cabinet">

            <?php $f = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

                <div class="head-user-cabinet"></div>

                <div class="user-avatar-block">
                    <div>
                        <img src="/web/images/users/<?= $user['img_user'] ?>" class="user-avatar"/>

                        <div class="">
                            <label for="add-avatar">
                                <div class="file-upload-label">
                                    <div class=''>
                                        UPLOAD
                                    </div>
                                </div>
                            </label>

                            <input type="file" id="add-avatar" name="avatar" class="add-avatar">
                            <?= $f->field($form, 'img_user')->textInput(['class' => 'add-avatar'])->label(false) ?>
                        </div>

                    </div>
                </div>

                <table>

                    <tr>
                        <td>Name</td>
                        <td><?= $f->field($form, 'name')->label(false)->textInput(['value' => $user->name]) ?></td>
                    </tr>

                    <tr>
                        <td>Middle name</td>
                        <td><?= $f->field($form, 'middle_name')->label(false)->textInput(['value' => $user->middle_name]) ?></td>
                    </tr>

                    <tr>
                        <td>Lastname</td>
                        <td><?= $f->field($form, 'last_name')->label(false)->textInput(['value' => $user->last_name]) ?></td>
                    </tr>

                    <tr>
                        <td><hr></td><td><hr></td>
                    </tr>

                    <tr>
                        <td>Display name:</td>
                        <td><?= $f->field($form, 'display_name')->label(false)->textInput(['value' => $user->display_name]) ?></td>
                    </tr>

                    <tr>
                        <td><hr></td><td><hr></td>
                    </tr>

                    <tr>
                        <td>Email:</td>
                        <td><?= $f->field($form, 'email')->label(false)->textInput(['value' => $user->email]) ?></td>
                    </tr>

                    <tr>
                        <td><hr></td><td><hr></td>
                    </tr>

                    <tr>
                        <td>Country:</td>
                        <td><?= $f->field($form, 'country')->label(false)->textInput(['value' => $user->country]) ?></td>
                    </tr>

                    <tr>
                        <td><hr></td><td><hr></td>
                    </tr>


                    <tr>
                        <td>City:</td>
                        <td><?= $f->field($form, 'street')->label(false)->textInput(['value' => $user->city]) ?></td>
                    </tr>

                    <tr>
                        <td>Street:</td>
                        <td><?= $f->field($form, 'street')->label(false)->textInput(['value' => $user->street]) ?></td>
                    </tr>

                    <tr>
                        <td>House:</td>
                        <td><?= $f->field($form, 'house')->label(false)->textInput(['value' => $user->house]) ?></td>
                    </tr>

                    <tr>
                        <td>Apartment:</td>
                        <td><?= $f->field($form, 'house')->label(false)->textInput(['value' => $user->apartment]) ?></td>
                    </tr>

                    <tr>
                        <td><hr></td><td><hr></td>
                    </tr>

                    <tr>
                        <td>Postal Code:</td>
                        <td><?= $f->field($form, 'postal_code')->label(false)->textInput(['value' => $user->postal_code]) ?></td>
                    </tr>

                    <tr>
                        <td><hr></td><td><hr></td>
                    </tr>

                    <tr>
                        <td>Phone:</td>
                        <td><?= $f->field($form, 'phone')->label(false)->textInput(['value' => $user->phone]) ?></td>
                    </tr>

                    <tr>
                        <td><hr></td><td><hr></td>
                    </tr>

                    <tr>
                        <td>Mobile phone:</td>
                        <td><?= $f->field($form, 'mobile_phone')->label(false)->textInput(['value' => $mobile_phone]) ?></td>
                    </tr>

                    <tr>
                        <td><hr></td><td><hr></td>
                    </tr>              
                </table>

                <div class="confirm-edit-form"><button class="confirm-edit-form" type="submit">CONFIRM</button></div>

            <?php ActiveForm::end() ?>

      
        <div class="head-user-cabinet"></div>
        </div>
    </div>

</section> <!--/#cart_items-->