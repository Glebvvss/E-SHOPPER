<?php use yii\widgets\ActiveForm; ?>
<?php use yii\helpers\Html; ?>

    <section id="form"><!--form-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="login-form"><!--login form-->
                        <h2>Login to your account</h2>

                        <?php $formLog = ActiveForm::begin([
                            'id' => 'login-form',
                            'fieldConfig' => [
                                'labelOptions' => ['class' => 'col-lg-1 control-label'],
                            ]
                        ]); ?>

                            <?=$formLog->field($loginFormModel, 'username')->textInput(['placeholder' => 'username'])->label(false)?>

                            <?=$formLog->field($loginFormModel, 'password')->textInput(['placeholder' => 'password', 'type' => 'password'])->label(false)?>

                            <span><?= $formLog->field($loginFormModel, 'rememberMe')->checkbox() ?></span>

                            <button type="submit" class="btn btn-default">Login</button>
                        <?php ActiveForm::end(); ?>



                    </div><!--/login form-->
                </div>
                        
<!--
                <div class="col-sm-1">
                    <h2 class="or">OR</h2>
                </div>
-->
                <div class="col-sm-4">
                    <div class="signup-form">
                        <h2>New User Signup!</h2>
                        <?php $formReg = ActiveForm::begin(); ?>
                            
                            <?= $formReg->field($singUpFormModel, 'username')->textInput(['placeholder' => 'username'])->label(false) ?>
                            
                            <?= $formReg->field($singUpFormModel, 'password')->textInput(['placeholder' => 'password'])->label(false) ?>
                            
                            <?= $formReg->field($singUpFormModel, 'confirm_password')->textInput(['placeholder' => 'confirm password'])->label(false) ?>

                            <button type="submit" class="btn btn-default">Signup</button> 

                            <div <?php  if($sing_up_note === 'Регистрация прошла успешно') ?> 
                                class="note-error" 
                            >

                                <?=$singUpNote?>
                            
                            </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

            </div>
        </div>
    </section>
