<section id="cart_items">
    <div class="container">
        
        <div class="user-cabinet">
            
            <div class="head-user-cabinet"></div>

            <div class="user-avater-block">
                <div>
                    <img src="/web/images/users/<?= $user['img_user'] ?>" class="user-avatar"/>
                </div>
            </div>

            <p><b><?=$user->name?> <?=$user->middle_name?> <?=$user->last_name?></b></p>

            <table>
                <tr>
                    <td>Display name:</td>
                    <td> <?= $user['display_name'] ?> </td>
                </tr>

                <tr>
                    <td><hr></td>
                    <td><hr></td>
                </tr>

                <tr>
                    <td>Email:</td>
                    <td><?= $user['email'] ?></td>
                </tr>

                <tr>
                    <td><hr></td>
                    <td><hr></td>
                </tr>
                <tr>
                    <td>Country:</td>
                    <td><?= $user['country'] ?></td>
                </tr>

                <tr>
                    <td><hr></td>
                    <td><hr></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td><?= $user['city'] ?> <?= $user['street'] ?> <?= $user['house'] ?> <?= $user['apartment'] ?> </td>
                </tr>

                <tr>
                    <td><hr></td>
                    <td><hr></td>
                </tr>
                <tr>
                    <td>Postal Code:</td>
                    <td><?= $user['postal_code'] ?></td>
                </tr>

                <tr>
                    <td><hr></td>
                    <td><hr></td>
                </tr>
                <tr>
                    <td>Phone:</td>
                    <td><?= $user['phone'] ?></td>
                </tr>

                <tr>
                    <td><hr></td>
                    <td><hr></td>
                </tr>
                <tr>
                    <td>Mobile phone:</td>
                    <td><?= $user['mobile_phone'] ?></td>
                </tr>

                <tr>
                    <td><hr></td>
                    <td><hr></td>
                </tr>              
            </table>

            <div class="edit-user">
                <a href="<?=yii::$app->urlManager->createUrl('site/edit')?>">Edit user information</a>
            </div>
            
        <div class="head-user-cabinet"></div>
        </div>

    </div>

</section> <!--/#cart_items-->