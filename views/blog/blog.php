<?php
	$this->title = "E-SHOPPER | Blog";
?>
<section id="article-page">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					
					<!-- Виджет меню категорий -->
                    <?=\app\components\MenuWidget::widget(['tpl' => 'menu'])?>
					<!-- Виджет писка брендов -->
					<?=\app\components\BrandsListWidget::widget(['brands' => $brands])?>	
					
					<div class="shipping text-center"><!--shipping-->
						<img src="/web/images/home/shipping.jpg" alt="" />
					</div><!--/shipping-->
				</div>
			</div>
			<div class="col-sm-9">
				<div class="blog-post-area">

					<!-- Виджет списка статей -->
					<?=\app\components\BlogWidget::widget(['blog' => $blog])?>
					
					<?php 
						echo yii\widgets\LinkPager::widget([
							'pagination' => $pages
						]);
					?>

				</div>
			</div>
		</div>
	</div>
</section>