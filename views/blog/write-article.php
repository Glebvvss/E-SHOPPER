<?php 
	use yii\widgets\ActiveForm;	
?>
<div class="container">	
	<h2 class="title text-center">Write article</h2>
	
	<div>
		<?php $f = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
			<?= $f->field($form, 'caption'); ?>
			<?= $f->field($form, 'article')->textarea(array('rows'=>11,'cols'=>5))->label('Text'); ?>
			<?= $f->field($form, 'img_article')->fileInput()->label('Image of article'); ?>
			<?= $f->field($form, 'tags') ?>
		<?php $f = ActiveForm::end(); ?>
	</div>
</div>