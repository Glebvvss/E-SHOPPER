<?php
	$this->title = "E-SHOPPER | " . $article['article']['caption'];
?>

<!--<section>-->
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					
					<!-- Виджет меню категорий -->
                    <?=\app\components\MenuWidget::widget(['tpl' => 'menu'])?>
                    <!-- Виджет писка брендов -->
					<?=\app\components\BrandsListWidget::widget(['brands' => $brands])?>
					
					<div class="shipping text-center"><!--shipping-->
						<img src="/web/images/home/shipping.jpg" alt="" />
					</div><!--/shipping-->
				</div>
			</div>
			<div class="col-sm-9">
			
				<!-- Виджет статьи из блога -->
				<?=\app\components\ArticleWidget::widget(['id_article' => $id_article, 'article' => $article, 'tags' => $tags])?>
				<!-- Виджет ывода комментариев -->
				<?=\app\components\CommentWidget::widget(['data' => $comments])?>							
			</div>	
		</div>
	</div>
<!--</section>-->
