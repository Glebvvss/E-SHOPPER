<?php if(!empty($session['cart'])) : ?>

	<div class="table-responsive">
		<table class="table table-hover table-striped">
			<tbody>
				<?php foreach($session['cart'] as $id => $cart) : ?>
					<tr>
						<td><img class="cart-img" src="/web/images/products/<?=$cart['img_product']?>"></td>
						<td><?=$cart['name_product']?></td>
						<td>$<?=$cart['price']?></td>
						<td class="pieces"><input data-id="<?=$id?>" class="pieces-input" type="text" value="<?=$cart['qty']?>"> pcs</td>
						<td><span style="cursor: pointer;" data-id="<?=$id?>" class="glyphicon glyphicon-remove text-danger del-item aria-hidden="true></span></td>
					</tr>
				<?php endforeach; ?>
				
			<tbody>
		</table>
		
		<table class="table table-hover table-striped">
			<tbody>
				<tr>
					<td>Quantity items: </td>
					<td class="cart-all"><?=$_SESSION['cart.qty']?></td>
				</tr>
				<tr>
					<td>Sum of all items: </td>
					<td class="cart-all">$<?=$_SESSION['cart.sum']?></td>
				</tr>
			</tbody>
		</table>

		<div>
			<ul class="cart-cart">
				<li><label>Delivery method</label></li>
				<li>
					<select class="select-cart" id="select-cart">
						<option value="0">--</option>
						<option value="pickup">Pickup from the store</option>
						<option value="home">Home delivery</option>
						<option value="post">From the post office</option>
					</select>
				</li>
			</ul>

			<button id="confirm-order" data-id="0" class="btn btn-primary button-cart">Confirm Order</button>
		</div>
	</div>


	<script>
		$('.select-delivery').hide();	
	</script>


	<!-- лок самовывоза -->
	<div class="select-delivery" id="pickup">	
		<div><label>Select store</label></div>
		<table class="table table-hover table-striped">						
			<tbody>											
				<tr>
					<td><input class="pickup" name="pickup" type="radio"></td>
					<td> 3522 Interstate 75 Business Spur, Sault </td>
				</tr>
				<tr>
					<td><input class="pickup" name="pickup" type="radio"></td>
					<td> 3522 Interstate 75 Business Spur, Sault </td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<!-- доставка на дом -->
	<div class="select-delivery" id="home">	
		<div><label>Confirm or update your addres</label></div>
		<table class="table table-hover table-striped">
			<tbody>			

		        <tr>
		            <td>Name</td>
		            <td><input class="cart-home" type="text"></td>
		        </tr>

		        <tr>
		            <td>Middle name</td>
		            <td><input class="cart-home" type="text"></td>
		        </tr>

		        <tr>
		            <td>Lastname</td>
		            <td><input class="cart-home" type="text"></td>
		        </tr>

	            <tr>
	                <td>Email:</td>
	                <td><input class="cart-home" type="text"></td>
	            </tr>

	            <tr>
	                <td>Email:</td>
	                <td><input class="cart-home" type="text"></td>
	            </tr>

	            <tr>
	                <td>Country:</td>
	                <td><input class="cart-home" type="text"></td>
	            </tr>

	            <tr>
	                <td>City:</td>
	                <td><input class="cart-home" type="text"></td>
	            </tr>

	            <tr>
	                <td>House:</td>
	                <td><input class="cart-home" type="text"></td>
	            </tr>      

	            <tr>
	                <td>Apartment:</td>
	                <td><input class="cart-home" type="text"></td>
	            </tr>  

	            <tr>
	                <td>Phone:</td>
	                <td><input class="cart-home" type="text"></td>
	            </tr>                                            

	            <tr>
	                <td>Mobile phone:</td>
	                <td><input class="cart-home" type="text"></td>
	            </tr>
	           
	        </tbody>
	    </table>
	</div>           

	<div class="select-delivery" id="post">	
		<div><label>Confirm or update</label></div>
		<table id="post" class="table table-hover table-striped">
			<tbody>			
				
				<tr>
		            <td>Name</td>
		            <td><input class="cart-home" type="text"></td>
		        </tr>

		        <tr>
		            <td>Middle name</td>
		            <td><input class="cart-home" type="text"></td>
		        </tr>

		        <tr>
		            <td>Lastname</td>
		            <td><input class="cart-home" type="text"></td>
		        </tr>

				<tr>
		            <td>Country</td>
		            <td><input class="cart-home" type="text"></td>
		        </tr>

		        <tr>
		            <td>City</td>
		            <td><input class="cart-home" type="text"></td>
		        </tr>

		        <tr>
		            <td>Index post</td>
		            <td><input class="cart-home" type="text"></td>
		        </tr>

			</tbody>
		</table>
	</div>

	<?php else: ?>
	<h3>Cart is empty</h3>
<?php endif; ?>