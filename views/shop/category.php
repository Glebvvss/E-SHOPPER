<?php
	$this->title = "E-SHOPPER | $category";
?>

<section id="advertisement">
	<div class="container">
		<img src="/web/images/shop/advertisement.jpg" alt="" />
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">

					<!-- Виджет меню категорий -->
                    <?=\app\components\MenuWidget::widget(['tpl' => 'menu'])?>
					<!-- Виджет писка брендов -->
					<?=\app\components\BrandsListWidget::widget(['brands' => $brands])?>	
					
					<div class="shipping text-center"><!--shipping-->
						<img src="/web/images/home/shipping.jpg" alt="" />
					</div><!--/shipping-->
					
				</div>
			</div>
			
			<div class="col-sm-9 padding-right">
				<div class="features_items"><!--features_items-->
					<!--Виджет содержания категорий товаров-->
					<?=app\components\CategoryItemsWidget::widget(['items' => $items, 'category' => $category])?>
					<!--Пагинация страницы-->
					<div class="down-block">
						<div class="center-pag">
							<?php
								echo yii\widgets\LinkPager::widget([
									'pagination' => $pages
								]);
							?>
						</div>
					</div>
				</div><!--features_items-->
			</div>
			
		</div>
	</div>
</section>